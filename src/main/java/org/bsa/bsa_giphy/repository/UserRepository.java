package org.bsa.bsa_giphy.repository;

import org.bsa.bsa_giphy.entity.Gif;
import org.bsa.bsa_giphy.entity.HistoryInstance;
import org.bsa.bsa_giphy.util.DiskHelper;
import org.bsa.bsa_giphy.util.GifHelper;
import org.bsa.bsa_giphy.util.UserHistoryCsvHelper;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class UserRepository {

    public List<Gif> getAllForUser(String id) {
        List<Gif> res = new ArrayList<>();

        String folderLoc = GifHelper.userLoc + "/" + id;
        File userFile = new File(folderLoc);

        if (!userFile.exists()) return Collections.emptyList();
        for (String query : userFile.list()) {
            if (new File(folderLoc + "/" + query).isDirectory()) {
                res.addAll(gifsByQuery(folderLoc, query));
            }
        }

        return res;
    }

    private List<Gif> gifsByQuery(String folderLoc, String query) {
        return Arrays.stream(new File(folderLoc + "/" + query).list())
                .map(id -> new Gif(id, query))
                .collect(Collectors.toList());
    }

    public List<HistoryInstance> getUserHistory(String id) {
        int datePos = UserHistoryCsvHelper.datePos;
        int queryPos = UserHistoryCsvHelper.queryPos;
        int pathPos = UserHistoryCsvHelper.pathPos;

        String folderLoc = GifHelper.userLoc + "/" + id;

        return UserHistoryCsvHelper.readFromCsv(folderLoc).stream()
                .skip(1)
                .map(inst -> new HistoryInstance(inst.get(datePos), inst.get(queryPos), inst.get(pathPos)))
                .collect(Collectors.toList());
    }

    public void deleteUserHistory(String id) {
        String folderLoc = GifHelper.userLoc + "/" + id;
        UserHistoryCsvHelper.clear(folderLoc);
    }

    public List<Gif> searchForUserByQuery(String id, String query) {
        String folderLoc = GifHelper.userLoc + "/" + id + "/" + query;
        File userQueryFile = new File(folderLoc);

        if (!userQueryFile.exists()) return Collections.emptyList();

        return Arrays.stream(userQueryFile.list())
                .map(gifId -> new Gif(gifId, query))
                .collect(Collectors.toList());
    }

    public void deleteUserGifsByQuery(String userId, String query) {
        String folderLoc = GifHelper.userLoc + "/" + userId + "/" + query;
        if (new File(folderLoc).exists()) {
            DiskHelper.clearFolder(folderLoc);
        }
    }
}
