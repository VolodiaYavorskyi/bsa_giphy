package org.bsa.bsa_giphy.repository;

import org.bsa.bsa_giphy.entity.Gif;
import org.bsa.bsa_giphy.util.DiskHelper;
import org.bsa.bsa_giphy.util.GifHelper;
import org.springframework.stereotype.Repository;

import java.io.File;
import java.util.*;
import java.util.stream.Collectors;

@Repository
public class CacheRepository {

    public List<Gif> getAllByQuery(String query) {
        List<Gif> res = new ArrayList<>();

        if (!query.isBlank()) {
            res.addAll(gifsByQuery(query));
        } else {
            for (String que : new File(GifHelper.cacheLoc).list()) {
                res.addAll(gifsByQuery(que));
            }
        }

        return res;
    }

    private List<Gif> gifsByQuery(String query) {
        String folderLoc = GifHelper.cacheLoc + "/" + query;
        String[] fileNames = new File(folderLoc).list();
        fileNames = fileNames == null ? new String[]{} : fileNames;

        return Arrays.stream(fileNames)
                .map(id -> new Gif(id, query))
                .collect(Collectors.toList());
    }

    public void clear() {
        DiskHelper.clearFolder(GifHelper.cacheLoc);
    }

    public List<Gif> getAll() {
        return new ArrayList<>(getAllByQuery(""));
    }
}
