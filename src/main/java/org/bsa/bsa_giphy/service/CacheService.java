package org.bsa.bsa_giphy.service;

import org.bsa.bsa_giphy.config.GiphyApp;
import org.bsa.bsa_giphy.dto.GifsByQueryDto;
import org.bsa.bsa_giphy.dto.QueryDto;
import org.bsa.bsa_giphy.entity.Gif;
import org.bsa.bsa_giphy.mapper.GifToDtoMapper;
import org.bsa.bsa_giphy.repository.CacheRepository;
import org.bsa.bsa_giphy.util.GifHelper;
import org.bsa.bsa_giphy.util.JsonHelper;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class CacheService {
    private final GiphyApp giphyApp;
    private final CacheRepository cacheRepository;
    private final GifToDtoMapper gifToDtoMapper;

    private static final String cacheLoc = GifHelper.cacheLoc;
    private static final String searchUrl = GifHelper.searchUrl;

    public CacheService(GiphyApp giphyApp, CacheRepository cacheRepository, GifToDtoMapper gifToDtoMapper) {
        this.giphyApp = giphyApp;
        this.cacheRepository = cacheRepository;
        this.gifToDtoMapper = gifToDtoMapper;
    }

    public List<GifsByQueryDto> getAllByQuery(String query) {
        return gifToDtoMapper.GifsToGifsByQueryList(cacheRepository.getAllByQuery(query));
    }

    public GifsByQueryDto generate(QueryDto queryDto) {
        String url = UriComponentsBuilder.fromUriString(searchUrl)
                .queryParam("api_key", giphyApp.getApiKey())
                .queryParam("tag", queryDto.getQuery())
                .build().toString();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        String responseBody = response.getBody();

        String id = JsonHelper.getJsonField(responseBody, "data", "id");
        String gifLink = GifHelper.gifUrlFromId(id);

        String folderLoc = cacheLoc + "/" + queryDto.getQuery();
        String saveLoc = folderLoc + "/" + id + ".gif";
        try {
            GifHelper.saveGifFromUrl(gifLink, saveLoc);
        } catch (IOException e) {
            e.printStackTrace();
        }

        return gifToDtoMapper.GifsToGifsByQuery(cacheRepository.getAllByQuery(queryDto.getQuery()));
    }

    public void delete() {
        cacheRepository.clear();
    }

    public List<String> getAll() {
        return cacheRepository.getAll().stream().map(Gif::getId).collect(Collectors.toList());
    }
}
