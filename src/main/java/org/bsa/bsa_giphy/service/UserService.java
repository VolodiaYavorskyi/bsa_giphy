package org.bsa.bsa_giphy.service;

import org.bsa.bsa_giphy.config.GiphyApp;
import org.bsa.bsa_giphy.dto.GifsByQueryDto;
import org.bsa.bsa_giphy.entity.Gif;
import org.bsa.bsa_giphy.entity.HistoryInstance;
import org.bsa.bsa_giphy.mapper.GifToDtoMapper;
import org.bsa.bsa_giphy.repository.CacheRepository;
import org.bsa.bsa_giphy.repository.UserRepository;
import org.bsa.bsa_giphy.util.GifHelper;
import org.bsa.bsa_giphy.util.JsonHelper;
import org.bsa.bsa_giphy.util.UserHistoryCsvHelper;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Random;

@Service
public class UserService {
    private final GiphyApp giphyApp;
    private final UserRepository userRepository;
    private final CacheRepository cacheRepository;
    private final GifToDtoMapper gifToDtoMapper;

    private static final String cacheLoc = GifHelper.cacheLoc;
    private static final String userLoc = GifHelper.userLoc;
    private static final String searchUrl = GifHelper.searchUrl;

    public UserService(GiphyApp giphyApp, UserRepository userRepository, CacheRepository cacheRepository,
                       GifToDtoMapper gifToDtoMapper) {
        this.giphyApp = giphyApp;
        this.userRepository = userRepository;
        this.cacheRepository = cacheRepository;
        this.gifToDtoMapper = gifToDtoMapper;
    }

    public List<GifsByQueryDto> getAllForUser(String id) {
        return gifToDtoMapper.GifsToGifsByQueryList(userRepository.getAllForUser(id));
    }

    public List<HistoryInstance> getUserHistory(String id) {
        return userRepository.getUserHistory(id);
    }

    public void deleteUserHistory(String id) {
        userRepository.deleteUserHistory(id);
    }

    public String searchByQuery(String id, String query, boolean force) {
        List<Gif> gifs = userRepository.searchForUserByQuery(id, query);

        Random rand = new Random();
        Gif randomElement = gifs.get(rand.nextInt(gifs.size()));

        return randomElement == null ? "" : randomElement.getId();
    }

    public String generateForUser(String userId, String query, boolean force) {
        initUser(userId);
        Gif gif = null;

        if (!force) {
            gif = searchInCacheByQuery(query);
        }
        if (gif == null) {
            gif = generateGif(query);
            saveGifInCache(gif.getId(), query);
        }

        copyFromCacheToUser(userId, gif.getId(), query);
        UserHistoryCsvHelper.writeToCsv(userLoc + "/" + userId, new String[]{
                LocalDate.now().toString(), query, gif.getId()});

        return gif.getId();
    }

    private void initUser(String id) {
        String userFolderLoc = userLoc + "/" + id;
        File file = new File(userFolderLoc);

        if (!file.exists()) {
            file.mkdirs();
        }

        UserHistoryCsvHelper.createFile(userFolderLoc);
    }

    private Gif searchInCacheByQuery(String query) {
        List<Gif> gifs = cacheRepository.getAllByQuery(query);

        if (gifs.size() == 0) {
            return null;
        }

        Random rand = new Random();
        return gifs.get(rand.nextInt(gifs.size()));
    }

    private Gif generateGif(String query) {
        String url = UriComponentsBuilder.fromUriString(searchUrl)
                .queryParam("api_key", giphyApp.getApiKey())
                .queryParam("tag", query)
                .build().toString();

        RestTemplate restTemplate = new RestTemplate();
        ResponseEntity<String> response = restTemplate.exchange(url, HttpMethod.GET, null, String.class);
        String responseBody = response.getBody();

        String gifId = JsonHelper.getJsonField(responseBody, "data", "id");

        return new Gif(gifId + ".gif", query);
    }

    private void saveGifInCache(String id, String query) {
        if (id.contains(".gif")) {
            id = id.replace(".gif", "");
        }

        String gifLink = GifHelper.gifUrlFromId(id);

        String cacheFolderLoc = cacheLoc + "/" + query;
        String cacheSaveLoc = cacheFolderLoc + "/" + id + ".gif";

        try {
            GifHelper.saveGifFromUrl(gifLink, cacheSaveLoc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void copyFromCacheToUser(String userId, String gifId, String query) {
        String cacheFolderLoc = cacheLoc + "/" + query;
        String userFolderLoc = userLoc + "/" + userId + "/" + query;

        File userFolder = new File(userFolderLoc);

        if (!userFolder.exists()) {
            userFolder.mkdirs();
        }

        Path cacheFilePath = Path.of(cacheFolderLoc + "/" + gifId);
        Path userFilePath = Path.of(userFolderLoc + "/" + gifId);

        try {
            Files.copy(cacheFilePath, userFilePath, StandardCopyOption.REPLACE_EXISTING);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void deleteUserGifs(String userId) {
        deleteUserGifsByQuery(userId, "");
    }

    public void deleteUserGifsByQuery(String userId, String query) {
        if (query.isBlank()) {
            String folderLoc = GifHelper.userLoc + "/" + userId;
            File userFile = new File(folderLoc);

            if (!userFile.exists()) return;

            for (String que : userFile.list()) {
                if (new File(folderLoc + "/" + que).isDirectory()) {
                    userRepository.deleteUserGifsByQuery(userId, que);
                }
            }
        } else {
            userRepository.deleteUserGifsByQuery(userId, query);
        }
    }
}
