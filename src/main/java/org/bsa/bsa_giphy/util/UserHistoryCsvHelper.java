package org.bsa.bsa_giphy.util;

import java.io.*;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class UserHistoryCsvHelper {
    public static final String fileName = "history.csv";
    public static final int datePos = 0;
    public static final int queryPos = 1;
    public static final int pathPos = 2;

    private UserHistoryCsvHelper() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    public static void createFile(String loc) {
        String fileLoc = loc + "/" + fileName;
        File file = new File(fileLoc);

        try {
            if (file.createNewFile()) {
                writeToCsv(loc, new String[]{"date", "query", "path"});
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void writeToCsv(String loc, String[] line) {
        String fileLoc = loc + "/" + fileName;
        File file = new File(fileLoc);

        try (PrintWriter pw = new PrintWriter(new FileWriter(file, true))) {
            String data = convertToCsv(line);
            pw.println(data);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static List<List<String>> readFromCsv(String loc) {
        List<List<String>> res = new ArrayList<>();
        String fileLoc = loc + "/" + fileName;
        File file = new File(fileLoc);

        if (!file.exists()) return Collections.emptyList();

        try (Scanner scanner = new Scanner(file)) {
            while (scanner.hasNextLine()) {
                String[] values = scanner.nextLine().split(",");
                res.add(Arrays.asList(values));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }

        return res;
    }

    private static String convertToCsv(String[] data) {
        return Stream.of(data).map(UserHistoryCsvHelper::escapeSpecialCharacters).collect(Collectors.joining(","));
    }

    private static String escapeSpecialCharacters(String data) {
        String escapedData = data.replaceAll("\\R", " ");
        if (data.contains(",") || data.contains("\"") || data.contains("\'")) {
            data = data.replace("\"", "\"\"");
            escapedData = "\"" + data + "\"";
        }
        return escapedData;
    }

    public static void clear(String loc) {
        String fileLoc = loc + "/" + fileName;
        File file = new File(fileLoc);

        if (file.delete()) {
            createFile(loc);
        }
    }
}
