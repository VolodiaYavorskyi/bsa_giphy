package org.bsa.bsa_giphy.util;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.FileOutputStream;
import java.net.URL;

public class GifHelper {
    public static final String cacheLoc = "cache";
    public static final String userLoc = "users";
    public static final String searchUrl = "https://api.giphy.com/v1/gifs/random";

    private GifHelper() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    public static String gifUrlFromId(String id) {
        return "https://i.giphy.com/media/" + id + "/giphy.gif";
    }

    public static void saveGifFromUrl(String gifUrl, String loc) throws IOException {
        URL url = new URL(gifUrl);

        new File(new File(loc).getParent()).mkdirs();

        try (InputStream is = url.openStream(); FileOutputStream fos = new FileOutputStream(loc)) {
            int data;
            while ((data = is.read()) != -1) {
                fos.write(data);
            }
        }
    }
}
