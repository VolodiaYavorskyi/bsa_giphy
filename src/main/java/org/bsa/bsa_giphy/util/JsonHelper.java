package org.bsa.bsa_giphy.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class JsonHelper {

    private JsonHelper() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    public static String getJsonField(String json, String... hierarchy) {
        ObjectMapper mapper = new ObjectMapper();
        JsonNode node = null;
        try {
            node = mapper.readTree(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }

        for (String elem : hierarchy) {
            if (elem.equals("FIRST")) {
                node = node.elements().next();
            } else {
                node = node.path(elem);
            }
        }

        return node == null ? "" : node.asText();
    }
}
