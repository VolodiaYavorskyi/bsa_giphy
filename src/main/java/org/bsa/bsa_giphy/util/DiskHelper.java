package org.bsa.bsa_giphy.util;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Comparator;

public class DiskHelper {

    private DiskHelper() {
        throw new UnsupportedOperationException("Not for instantiation");
    }

    public static void clearFolder(String loc) {
        try {
            Files.walk(Path.of(loc)).skip(1).sorted(Comparator.reverseOrder()).map(Path::toFile).forEach(File::delete);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
