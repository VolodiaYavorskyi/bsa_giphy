package org.bsa.bsa_giphy.filter;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.GenericFilterBean;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@Component
public class RequestFilter extends GenericFilterBean {
    private static final Logger LOG = LogManager.getLogger(RequestFilter.class);
    private static final String BSA_HEADER = "X-BSA-GIPHY";


    @Override
    public void doFilter(ServletRequest servletRequest,
                         ServletResponse servletResponse,
                         FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;

        String bsaHeader = req.getHeader(BSA_HEADER);

        if (bsaHeader == null) {
            HttpServletResponse resp = (HttpServletResponse) servletResponse;
            String error = "No " + BSA_HEADER + " header";
            resp.reset();
            resp.setStatus(HttpServletResponse.SC_UNAUTHORIZED);
            resp.setContentLength(error.length());
            resp.getWriter().write(error);

            return;
        }

        LOG.info("new request: {}", req.getRequestURI());
        filterChain.doFilter(servletRequest, servletResponse);
    }
}
