package org.bsa.bsa_giphy.controller;

import org.bsa.bsa_giphy.dto.GifsByQueryDto;
import org.bsa.bsa_giphy.dto.QueryForceDto;
import org.bsa.bsa_giphy.entity.HistoryInstance;
import org.bsa.bsa_giphy.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class ApiUserController {
    private final UserService userService;

    @Autowired
    public ApiUserController(UserService userService) {
        this.userService = userService;
    }

    @GetMapping("/{id}/all")
    public List<GifsByQueryDto> getAllUserGifs(@PathVariable String id) {
        return userService.getAllForUser(id);
    }

    @GetMapping("/{id}/history")
    public List<HistoryInstance> getUserHistory(@PathVariable String id) {
        return userService.getUserHistory(id);
    }

    @DeleteMapping("/{id}/history/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserHistory(@PathVariable String id) {
        userService.deleteUserHistory(id);
    }

    @GetMapping("/{id}/search")
    public String searchByQuery(@PathVariable String id, @RequestParam String query,
                                @RequestParam(defaultValue = "false") boolean force) {
        return userService.searchByQuery(id, query, force);
    }

    @PostMapping("{id}/generate")
    public String generateForUser(@PathVariable String id, @RequestBody QueryForceDto queryForceDto) {
        return userService.generateForUser(id, queryForceDto.getQuery(), queryForceDto.isForce());
    }

    @DeleteMapping("/{id}/reset")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserGifsByQuery(@PathVariable String id, @RequestParam(defaultValue = "") String query) {
        userService.deleteUserGifsByQuery(id, query);
    }

    @DeleteMapping("/{id}/clean")
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteUserGifs(@PathVariable String id) {
        userService.deleteUserGifs(id);
    }
}
