package org.bsa.bsa_giphy.controller;

import org.bsa.bsa_giphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class ApiController {
    private final CacheService cacheService;

    @Autowired
    public ApiController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping("/gifs")
    public List<String> getAllCacheGiffs() {
        return cacheService.getAll();
    }
}
