package org.bsa.bsa_giphy.controller;

import org.bsa.bsa_giphy.dto.GifsByQueryDto;
import org.bsa.bsa_giphy.dto.QueryDto;
import org.bsa.bsa_giphy.service.CacheService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cache")
public class ApiCacheController {
    private final CacheService cacheService;

    @Autowired
    public ApiCacheController(CacheService cacheService) {
        this.cacheService = cacheService;
    }

    @GetMapping
    public List<GifsByQueryDto> getGifsByQuery(@RequestParam(defaultValue = "") String query) {
        return cacheService.getAllByQuery(query);
    }

    @PostMapping("/generate")
    public GifsByQueryDto generateGifByQuery(@RequestBody QueryDto queryDto) {
        return cacheService.generate(queryDto);
    }

    @DeleteMapping
    @ResponseStatus(code = HttpStatus.NO_CONTENT)
    public void deleteCache() {
        cacheService.delete();
    }
}
