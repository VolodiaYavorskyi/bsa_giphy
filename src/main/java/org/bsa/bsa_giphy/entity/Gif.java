package org.bsa.bsa_giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class Gif {
    private String id;
    private String query;
}
