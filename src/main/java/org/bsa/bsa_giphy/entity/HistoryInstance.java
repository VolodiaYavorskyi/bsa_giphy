package org.bsa.bsa_giphy.entity;

import lombok.AllArgsConstructor;
import lombok.Data;

@AllArgsConstructor
@Data
public class HistoryInstance {
    private String date;
    private String query;
    private String gif;
}
