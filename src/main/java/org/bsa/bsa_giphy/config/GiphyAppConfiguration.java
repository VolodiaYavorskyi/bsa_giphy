package org.bsa.bsa_giphy.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class GiphyAppConfiguration {
    @Bean
    public GiphyApp giphyApp(GiphyAppConfigurationProperties configurationProperties) {
        return new GiphyApp(configurationProperties.getApiKey());
    }
}
