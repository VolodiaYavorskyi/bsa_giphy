package org.bsa.bsa_giphy.config;

import lombok.Value;

@Value
public class GiphyApp {
    private final String apiKey;
}
