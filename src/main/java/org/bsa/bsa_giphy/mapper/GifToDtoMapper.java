package org.bsa.bsa_giphy.mapper;

import org.bsa.bsa_giphy.dto.GifsByQueryDto;
import org.bsa.bsa_giphy.entity.Gif;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Component
public class GifToDtoMapper {

    public GifsByQueryDto GifsToGifsByQuery(List<Gif> gifs) {
        return new GifsByQueryDto(gifs.get(0).getQuery(), gifs.stream().map(Gif::getId).collect(Collectors.toList()));
    }

    public List<GifsByQueryDto> GifsToGifsByQueryList(List<Gif> gifs) {
        List<GifsByQueryDto> res = new ArrayList<>();

        Map<String, List<String>> gifMap = gifs.stream()
                .collect(Collectors.groupingBy(Gif::getQuery,
                        Collectors.mapping(Gif::getId, Collectors.toList())));

        for (var entry : gifMap.entrySet()) {
            res.add(new GifsByQueryDto(entry.getKey(), entry.getValue()));
        }

        return res;
    }
}
